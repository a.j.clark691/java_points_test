INSERT INTO ROUTES (airportOne, airportTwo, points) VALUES('LHR','LAX',4500);
INSERT INTO ROUTES (airportOne, airportTwo, points) VALUES('LHR','SFO',4400);
INSERT INTO ROUTES (airportOne, airportTwo, points) VALUES('LHR','JFK',3200);
INSERT INTO ROUTES (airportOne, airportTwo, points) VALUES('LGW','YYZ',3250);

INSERT INTO CABINS VALUES('F',100);
INSERT INTO CABINS VALUES('J',50);
INSERT INTO CABINS VALUES('W',20);
INSERT INTO CABINS VALUES('M',0);
