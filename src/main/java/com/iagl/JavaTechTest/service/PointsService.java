package com.iagl.JavaTechTest.service;

import com.iagl.JavaTechTest.model.Route;
import com.iagl.JavaTechTest.model.RoutePoints;
import com.iagl.JavaTechTest.repo.CabinsRepository;
import com.iagl.JavaTechTest.repo.RoutesRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PointsService {

    private final RoutesRepository routesRepository;
    private final CabinsRepository cabinsRepository;

    private static final int BASE_POINTS = 500;

    public PointsService(RoutesRepository routesRepository, CabinsRepository cabinsRepository) {
        this.routesRepository = routesRepository;
        this.cabinsRepository = cabinsRepository;
    }

    public RoutePoints getPointsForRoute(String origin, String destination, String cabin) {

        RoutePoints routePoints = new RoutePoints();

        double basePoints = BASE_POINTS;

        Optional<Route> route = routesRepository.findByAirportOneAndAirportTwo(origin, destination)
                .or(() -> routesRepository.findByAirportOneAndAirportTwo(destination, origin)).stream()
                .findFirst();

        if (route.isPresent()) {
            basePoints = route.get().getPoints();
        }

        if (null == cabin) {
            routePoints.addCabinPoints("F", getFinalPoints(basePoints, "F"));
            routePoints.addCabinPoints("J", getFinalPoints(basePoints, "J"));
            routePoints.addCabinPoints("W", getFinalPoints(basePoints, "W"));
            routePoints.addCabinPoints("M", getFinalPoints(basePoints, "M"));
        } else {
            routePoints.addCabinPoints(cabin, getFinalPoints(basePoints, cabin));
        }

        return routePoints;
    }

    private double getFinalPoints(double basePoints, String cabin) {
        double percentageIncrease = cabinsRepository.findById(cabin).orElseThrow().getBonus() / 100;
        return basePoints + (basePoints * percentageIncrease);
    }
}
