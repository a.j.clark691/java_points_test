package com.iagl.JavaTechTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaTechTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaTechTestApplication.class, args);
    }

}
