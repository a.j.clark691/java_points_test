package com.iagl.JavaTechTest.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Routes")
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "AIRPORTONE", nullable = false)
    private String airportOne;
    @Column(name = "AIRPORTTWO", nullable = false)
    private String airportTwo;
    @Column(name = "points", nullable = false)
    private int points;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAirportOne() {
        return airportOne;
    }

    public void setAirportOne(String airportOne) {
        this.airportOne = airportOne;
    }

    public String getAirportTwo() {
        return airportTwo;
    }

    public void setAirportTwo(String airportTwo) {
        this.airportTwo = airportTwo;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
