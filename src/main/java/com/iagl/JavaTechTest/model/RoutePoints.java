package com.iagl.JavaTechTest.model;

import java.util.HashMap;
import java.util.Map;

public class RoutePoints {

    private final Map<String, Double> cabinPoints = new HashMap<>();

    public Map<String, Double> getCabinPoints() {
        return cabinPoints;
    }

    public void addCabinPoints(String cabin, Double points) {
        this.cabinPoints.put(cabin, points);
    }
}
