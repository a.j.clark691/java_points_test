package com.iagl.JavaTechTest.controllers;

import com.iagl.JavaTechTest.model.RoutePoints;
import com.iagl.JavaTechTest.service.PointsService;
import jakarta.validation.constraints.Size;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PointsController {

    public PointsController(PointsService pointsService) {
        this.pointsService = pointsService;
    }

    private final PointsService pointsService;

    @GetMapping("points")
    @Validated
    public ResponseEntity<RoutePoints> getPoints( @RequestParam("origin") @Size(min = 3, max = 3, message = "Code must be 3 characters") String origin, @Size(min = 3, max = 3, message = "Code must be 3 characters") @RequestParam("destination") String destination, @Nullable @Size(min = 1, max = 1, message = "Code must be 1 character") @RequestParam("cabin") String cabin) {
        RoutePoints routePoints = pointsService.getPointsForRoute(origin.toUpperCase(), destination.toUpperCase(), cabin);
        return new ResponseEntity<>(routePoints, HttpStatus.OK);
    }
}
