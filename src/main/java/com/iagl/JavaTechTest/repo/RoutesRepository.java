package com.iagl.JavaTechTest.repo;

import com.iagl.JavaTechTest.model.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoutesRepository extends JpaRepository<Route, Long> {

    Optional<Route> findByAirportOneAndAirportTwo(String airportOne, String airportTwo);

}
