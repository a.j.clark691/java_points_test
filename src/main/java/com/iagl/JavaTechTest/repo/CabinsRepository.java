package com.iagl.JavaTechTest.repo;

import com.iagl.JavaTechTest.model.Cabin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CabinsRepository extends JpaRepository<Cabin, String> {
}
